/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package konyvtarapp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



/**
 *
 * @author Patrik
 */
public class Konyvtar extends javax.swing.JFrame {

    
    private List<Konyv> konyvtar = new ArrayList<>();
    
    public Konyvtar() {
        initComponents();
        
        File file = new File("pelda.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(file);
            xml.normalize();
            NodeList konyvNodeok = xml.getElementsByTagName("konyv");
            for(Integer i = 0; i < konyvNodeok.getLength(); i++){
                Node whiskyNode = konyvNodeok.item(i);
                Element konyvElement = (Element)whiskyNode;
                String mufaj = konyvElement.getElementsByTagName("mufaj").item(0).getTextContent();
                String szerzo = konyvElement.getElementsByTagName("szerzo").item(0).getTextContent();
                String cim = konyvElement.getElementsByTagName("cim").item(0).getTextContent();
                String sorozat = konyvElement.getElementsByTagName("cim").item(0).getAttributes().getNamedItem("sorozat").getNodeValue();
                String polc = konyvElement.getElementsByTagName("polc").item(0).getTextContent();
                String allapot = konyvElement.getElementsByTagName("allapot").item(0).getTextContent();
                Konyv k = new Konyv(mufaj, szerzo, cim, sorozat, polc, allapot);
                this.konyvtar.add(k);
            }
            
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        
        
        DefaultTableModel dtm = new DefaultTableModel(0, 0);
            String header[] = new String [] {"Műfaj", "Szerző", "Cím", "Sorozat", "Polc", "Állapot"};
            dtm.setColumnIdentifiers(header);
            this.konyvElemek.setModel(dtm);
            for(Konyv konyv: this.konyvtar){
               
                dtm.addRow(new String[] { konyv.getMufaj(), konyv.getSzerzo(), konyv.getCim(), konyv.getSorozat(), konyv.getPolc(), konyv.getAllapot() });
            }
            this.konyvElemek.setEnabled(Boolean.FALSE);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        konyvElemek = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        konyvElemek.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Műfaj", "Szerző", "Cím", "Sorozat", "Polc", "Állapot"
            }
        ));
        jScrollPane1.setViewportView(konyvElemek);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(219, 219, 219)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(262, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(92, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(210, 210, 210))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Konyvtar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Konyvtar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Konyvtar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Konyvtar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Konyvtar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable konyvElemek;
    // End of variables declaration//GEN-END:variables
}
