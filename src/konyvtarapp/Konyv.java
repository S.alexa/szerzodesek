
package konyvtarapp;

/**
 *
 * @author Patrik
 */
public class Konyv {
    String mufaj;
    String szerzo;
    String cim;
    String sorozat;
    String polc;
    String allapot;

    public Konyv(String mufaj, String szerzo, String cim, String sorozat, String polc, String allapot) {
        this.mufaj = mufaj;
        this.szerzo = szerzo;
        this.cim = cim;
        this.sorozat = sorozat;
        this.polc = polc;
        this.allapot = allapot;
    }

    public void setPolc(String polc) {
        this.polc = polc;
    }

    public void setAllapot(String allapot) {
        this.allapot = allapot;
    }

    public String getMufaj() {
        return mufaj;
    }

    public String getSzerzo() {
        return szerzo;
    }

    public String getCim() {
        return cim;
    }
    
     public String getSorozat() {
        return sorozat;
    }

    public String getPolc() {
        return polc;
    }

    public String getAllapot() {
        return allapot;
    }
    
    
    
}
